class TestBugJ1 {
    public static void main(String[] a) {
	System.out.println(new Test().f());
    }
}

class Test {
	//sets up x as int and y as boolean
    public int f() {
	int x;
	boolean y;
	x = 10;
	y = true;
	x += y;
	//prints out the result
	return x;
    }

}
