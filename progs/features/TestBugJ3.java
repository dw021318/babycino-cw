class TestBugJ3 {
	//prints out 40 if there are modifications in x
    public static void main(String[] a) {
	System.out.println(new Test().f());
    }
}

class Test {

	public int f() {
		//sets x as an int = 10
		int x;
		int y;
		x = 10;
		y = 30;
		//checks for modifications of x
		x += y;
		//prints out value of x
		return x;
	}


}
